import React, {useState} from 'react';
import { nanoid } from 'nanoid';
import {
    Box,
    Button, Chip,
    Container,
    CssBaseline,
    Grid,
    IconButton,
    InputAdornment,
    makeStyles, MenuItem,
    Paper,
    TextField
} from "@material-ui/core";
import DeleteIcon from '@material-ui/icons/Delete';

const CATEGORIES = ["Entertainment", "Car", "Food"];

const COLORS = {
    "Entertainment": '#FF9900',
    "Car": '#00FFFf',
    "Food": '#6AA04F'
}

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(4)
    },
    legendColor: {
        width: '50px',
        height: '50px',
        display: 'inline-block'
    }
}));

const App = () => {
    const classes = useStyles();

    const [items, setItems] = useState([
        {name: 'Milk', id: nanoid(), price: 50, category: CATEGORIES[2]},
        {name: 'Bottle of water', id: nanoid(), price: 25, category: CATEGORIES[2]},
        {name: 'Fuel', id: nanoid(), price: 1500, category: CATEGORIES[1]},
        {name: 'Movie', id: nanoid(), price: 600, category: CATEGORIES[0]}
    ]);

    const [name, setName] = useState("");
    const [price, setPrice] = useState("");
    const [category, setCategory] = useState(CATEGORIES[0]);

    // console.log(name);

    const addItem = e => {
      e.preventDefault();
      setItems([
          ...items,
          {
              id: nanoid(),
              name: name,
              price: parseFloat(price),
              category
          }
      ]);

        console.log(items)
        setName("");
        setPrice("");
    };

    const removeItem = id => {
        const index = items.findIndex(i => i.id === id);
        const itemsCopy = [...items];
        itemsCopy.splice(index, 1);
        setItems(itemsCopy);
    }

    const totalSpent = () => {
        return items.reduce((acc, item) => {
            return acc + item.price;
        }, 0)
    }

    const getSpentByCategory = () => {
        const total = totalSpent();
        const spentByCategory = items.reduce((acc, i) => {
            if(acc[i.category]) {
                acc[i.category] += i.price;
            } else {
                acc[i.category] = i.price;
            }

            return acc
        }, {})

        return CATEGORIES.reduce((acc, c) => {
            const percent = (spentByCategory[c] / total) * 100;
            if (!!spentByCategory[c]){
                acc.push({
                    name: c,
                    percent,
                    color: COLORS[c]
                });
            }
            return acc
        }, [])
    };

    return (
       <Container maxWidth="md" className={classes.root}>
           <CssBaseline/>
           <form onSubmit={addItem}>
               <Grid container spacing={2} alignItems="center">
                   <Grid item>
                       <TextField
                           label="Item name"
                           variant="outlined"
                           value={name}
                           onChange={e => setName(e.target.value)}
                       />
                   </Grid>

                   <Grid item>
                       <TextField
                           label="Cost"
                           variant="outlined"
                           InputProps={{
                               endAdornment: <InputAdornment position="end">KGS</InputAdornment>,
                           }}
                           value={price}
                           onChange={e => setPrice(e.target.value)}
                       />
                   </Grid>

                   <Grid item>
                       <TextField
                           select
                           label="Category"
                           variant="outlined"
                           value={category}
                           onChange={e => setCategory(e.target.value)}
                       >
                           {CATEGORIES.map(c => (
                               <MenuItem key={c} value={c}>
                                   {c}
                               </MenuItem>
                           ))}
                       </TextField>
                   </Grid>

                   <Grid item>
                       <Button variant="contained" color="primary" type="submit">
                           Add
                       </Button>
                   </Grid>
               </Grid>
           </form>

           <Grid container direction="column" spacing={2} className={classes.root}>
               {items.map(item => (
                   <Grid
                       item
                       key={item.id}
                   >
                       <Paper component={Box} p={1}>
                           <Grid container justify="space-between" alignItems="center">
                               <Grid item>
                                   {item.name}{" "}
                                   <Chip
                                       size="small"
                                       label={item.category}
                                       style={{background: COLORS[item.category]}}
                                   />
                               </Grid>

                               <Grid item>
                                   {item.price} KGS
                                   <IconButton onClick={() => removeItem(item.id)}>
                                       <DeleteIcon />
                                   </IconButton>
                               </Grid>
                           </Grid>
                       </Paper>
                   </Grid>
               ))}
           </Grid>

           <Paper component={Box} p={3} mt={3}>
               <strong>Total spent: {totalSpent()}</strong>
               <Grid container>
                   {getSpentByCategory().map(c => (
                       <Grid
                           item
                           key={c.name}
                           style={{
                               background: c.color,
                               width: c.percent + "%",
                               height: "50px"
                           }}
                       />
                   ))}
               </Grid>
               <ul>
                   {CATEGORIES.map((c) =>(
                       <li key={c}>
                           <div style={{background: COLORS[c]}} className={classes.legendColor}/>
                           {c}
                       </li>
                   ))}
               </ul>
           </Paper>

       </Container>
    );
};

export default App;